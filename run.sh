#!/bin/bash
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TARGETS="$*"

# --- Environment setup
UNSERVED_SRC=${UNSERVED_SRC:-}
UNSERVED_JAR=${UNSERVED_JAR:-}
FORCE_WSCCONV=${FORCE_WSCCONV:-}
FORCE_SHADOWGEN=${FORCE_SHADOWGEN:-}
WSC_ORIGINAL=${WSC_ORIGINAL:-}

OVR_UNSERVED_SRC=$UNSERVED_SRC
OVR_UNSERVED_JAR=$UNSERVED_JAR
OVR_FORCE_WSCCONV=$FORCE_WSCCONV
OVR_FORCE_SHADOWGEN=$FORCE_SHADOWGEN
OVR_WSC_ORIGINAL=$WSC_ORIGINAL

# Try to load variables from env.sh
if [ -f "$SCRIPTDIR/env.sh" ]; then
  source "$SCRIPTDIR/env.sh"
  # Override env.sh with actual previous environment
  test -z "$OVR_UNSERVED_SRC" || UNSERVED_SRC=$OVR_UNSERVED_SRC
  test -z "$OVR_UNSERVED_JAR" || UNSERVED_JAR=$OVR_UNSERVED_JAR
  test -z "$OVR_FORCE_WSCCONV"   || FORCE_WSCCONV=$OVR_FORCE_WSCCONV
  test -z "$OVR_FORCE_SHADOWGEN" || FORCE_SHADOWGEN=$OVR_FORCE_SHADOWGEN
  test -z "$OVR_WSC_ORIGINAL" || WSC_ORIGINAL=$OVR_WSC_ORIGINAL
fi

# Apply defaults
test -n "$UNSERVED_SRC" || UNSERVED_SRC=$SCRIPTDIR/unserved-testbench
test -n "$UNSERVED_JAR" || UNSERVED_JAR=$UNSERVED_SRC/target/unserved-testbench-1.0-SNAPSHOT.jar
test -n "$FORCE_WSCCONV"   || FORCE_WSCCONV=no
test -n "$FORCE_SHADOWGEN" || FORCE_SHADOWGEN=no
test -n "$WSC_ORIGINAL" || WSC_ORIGINAL="$SCRIPTDIR/sws-test-collections/src/main/resources/services/wsc08/"

export UNSERVED_SRC
export UNSERVED_JAR
export SLEEP


# Validate some variables
if ! ( echo $FORCE_WSCCONV | grep -E '(yes|no)' &>/dev/null ); then
  echo "FORCE_WSCCONV must be either yes or no. \"$FORCE_WSCCONV\" was given"; 
  exit 1
fi
if ! ( echo $FORCE_SHADOWGEN | grep -E '(yes|no)' &>/dev/null ); then
  echo "FORCE_SHADOWGEN must be either yes or no. \"$FORCE_SHADOWGEN\" was given"; 
  exit 1
fi

# Hardcoded values
SHADOWPRECOND_CLASSNAME='br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.ShadowPreconditionAssigner$App'
WSCCONVERTER_CLASSNAME=br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.WscProblemConverterApp
PPCONVERTER_CLASSNAME=br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.PragmaticProofConverter
WSC_CONVERTED="$SCRIPTDIR/wsc-converted"

# --- fix a cpu clock speed
set-cpufreq.sh && CLOCKSET=1 || echo "Failed to set a cpu clock speed, ignoring"

# --- download unserved-testbench, if needed
if [ ! -d "${UNSERVED_SRC}" ]; then
    pushd "${SCRIPTDIR}"
    git clone --depth 1 https://bitbucket.org/alexishuf/unserved-testbench.git || exit 1
    pushd unserved-testbench || exit 1
    git config submodule."src/main/resources/unserved".url http://bitbucket.org/alexishuf/unserved.git
    git submodule update --init --recursive || exit 1
    git checkout origin/master || exit 1
    popd
    UNSERVED_SRC="${SCRIPTDIR}/unserved-testbench"
    UNSERVED_JAR="${UNSERVED_SRC}/target/unserved-testbench-1.0-SNAPSHOT.jar"
    popd
fi

# --- package unserved-testbench
if [ ! -z "${UNSERVED_SRC}" ]; then
  pushd "${UNSERVED_SRC}"
  mvn -DskipTests=true package || exit 1
  popd
fi

# --- convert WSC'08 problems, if needed
if [ ! -d "${WSC_CONVERTED}" -o "$FORCE_WSCCONV" = 'yes' ]; then
    if [ ! -d "${WSC_ORIGINAL}" ]; then
	echo "WSC'08 files not found at ${WSC_ORIGINAL}, downloading..."
	pushd "${SCRIPTDIR}"
	git clone --depth 1 "https://github.com/kmi/sws-test-collections.git" || exit 1
	WSC_ORIGINAL="${SCRIPTDIR}/sws-test-collections/src/main/resources/services/wsc08"
	popd
    fi

    echo "uNSERVED WSC'08 files not found at ${WSC_CONVERTED}, converting..."
    mkdir -p "${WSC_CONVERTED}" || exit 1
    java -cp "${UNSERVED_JAR}" $WSCCONVERTER_CLASSNAME --wsc-root "${WSC_ORIGINAL}" \
	 --out-root "${WSC_CONVERTED}"
    if [ ! $? -eq 0 ]; then
	echo "Failed to convert WSC'08 problems"; exit 1
    fi
fi

# --- add shadow preconditions, if needed
function gen_shadow {
  FILE=$1
  FORCE=$2
  if [ ! -f "$FILE" ]; then echo "File $FILE does not exist"; exit 1; fi
  BASE=$(basename $1 | sed -E 's/^(.*)\.json$/\1/I')
  NOSL=$(echo $WSC_CONVERTED | sed -E 's@/$@@')
  DIR="$NOSL+$BASE"
  if [ ! -e "$DIR" -o "$FORCE" = "yes" ]; then
    rm -f "$DIR"
    cp -r "$WSC_CONVERTED" "$DIR"
    java -cp "$UNSERVED_JAR" $SHADOWPRECOND_CLASSNAME --config "$FILE" "$DIR" || exit 1
  fi
}
pushd "$SCRIPTDIR"
for i in shadow_conditions/*.json; do
  gen_shadow "$i" "$FORCE_SHADOWGEN"
done
popd

# --- convert WSC on Unserved to WSC on PP, with some variants
function gen-wsc-pp {
  NAME=$1
  SHADOW=$2
  if [ ! -d "$SCRIPTDIR/$NAME" ]; then
    java -cp "$UNSERVED_JAR" $PPCONVERTER_CLASSNAME --in "$WSC_CONVERTED" \
         --out "$SCRIPTDIR/$NAME" --without-http --shadow $SHADOW
    if [ "$?" -ne 0 ]; then
      echo "Failed to convert wsc-converted into $NAME"
      rm -fr "$SCRIPTDIR/$NAME"
      exit 1
    fi
  fi
}
gen-wsc-pp wsc-pp-converted NONE
gen-wsc-pp wsc-pp-converted-dup DUP_IOS
gen-wsc-pp wsc-pp-converted-single SINGLE_CONJUNCTION
gen-wsc-pp wsc-pp-converted-shadow SHADOW_CONJUNCTION

# --- run tests
function run-design {
  RUNNER="$1"
  ARG="$2"
  FOUND=no
  for i in $(echo $TARGETS); do 
    test "$(echo $i | sed 's@/$@@')" = "$ARG" && FOUND=yes && break
  done
  if [ "$FOUND" = no ]; then
    return 0;
  else
    pushd "$SCRIPTDIR" && pushd $ARG || return 1
    "../runners/$RUNNER" 
    RET=$?
    popd ; popd
    return $RET
  fi
}

run-design run-wsc.sh design_1
run-design run-wsc.sh design_2
run-design run-wsc.sh design_3
run-design run-wsc.sh design_4

run-design run-eye.sh design_pp-wsc

# --- restore cpu clock
if [ ! -z "${CLOCKSET}" ]; then
   unset-cpufreq.sh
fi

# --- compile .Rmd
# TODO implement
