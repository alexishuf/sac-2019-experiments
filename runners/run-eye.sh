#!/bin/bash
RUNDIR="$(pwd)"

# --- Environment setup
SLEEP=${SLEEP:-1000}
SLEEP_SECS=$(($SLEEP/1000))

echo "run-eye.sh Environment"
echo "--------------------------"
echo "RUNDIR=$RUNDIR"
echo "SLEEP=$SLEEP"
echo "--------------------------"

# Hardcoded
OUTDIR="$RUNDIR/run"
CLASSNAME=br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.WscRawExperimentRunner

# Validate some variables

# --- cleanup run dir if running from scratch
echo "Starting experiment from scratch"
rm -fr "$OUTDIR"
rm -f "results.csv"
mkdir -p "$OUTDIR"

# --- check for EYE
if ! ( eye &> /dev/null ); then
  echo "Failed to run EYE!"
  exit 1
fi
eye &>/dev/null | head -n 2 > "$OUTDIR/eye.version"

# --- check for design.csv
if ( ! head -n 1 "$RUNDIR/design.csv" | grep -E '^"?name"?, *"?rep"?, *"?problem"?, *"?wscConvertedRoot' &>/dev/null ); then
  echo "Bad design file, first columns should be name,rep,problem,wscConvertedRoot"
  exit 1
fi
echo "$(head -n 1 "$RUNDIR/design.csv"),reasoning" > results.csv

# --- run
EYE_CMD="eye initial.n3 taxonomy.ttl inf.n3 descriptions.n3 --query goal.n3"
tail -n +2 "$RUNDIR/design.csv" | while read -r line ; do 
  REP=$(echo $line | cut -f2 -d, | sed 's/ *//g')
  PROBLEM=$(echo $line | cut -f3 -d, | sed 's/ *//g')
  CONV_ROOT=$(echo $line | cut -f4 -d, | sed 's/^ *//' | sed 's/ *$//' | sed 's/"//g')
  PROBLEM_DIR="$CONV_ROOT/0$PROBLEM"
  echo "Running rep=$REP, problem=$PROBLEM, wscConvertedRoot=$CONV_ROOT ..."

  # Check dir exists
  if [ ! -d "$PROBLEM_DIR" ]; then
    echo "No directory $PROBLEM_DIR found!"
    # store NA as result
    echo "$line," >> results.csv
    continue
  fi

  # Run
  sh -c "cd \"$PROBLEM_DIR\" && $EYE_CMD 1>/dev/null 2>\"$OUTDIR/tmp\"" &
  PID=$!
  for i in $(seq 1200); do # wait at most 5 mins
    sleep 0.25s
    ps -p $PID >/dev/null || break
  done
  if ( ps -p $PID >/dev/null ); then
    # Kill
    echo "Timeout of 5min for $PID, command was $EYE_CMD"
    FIRSTKILL=1
    while ( killall swipl ); do
      if [ "$FIRSTKILL" -ne 1 ]; then
        echo "doing killall swipl again..."
      fi
      sleep 1s
      FIRSTKILL=0
    done
    # store NA as results
    echo "$line," >> results.csv
  else
    # fetch reasoning time
    MS=$(sed -n -E 's/^reasoning ([0-9]+) \[msec cputime\] ([0-9]+) \[msec walltime\].*$/\2/p' "$OUTDIR/tmp")
    echo "reasoning=$MS"
    echo "$line,$MS" >> results.csv
  fi

  # Cooldown
  sleep "$SLEEP_SECS"
done

exit 0
