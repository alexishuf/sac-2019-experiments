#!/bin/bash
RUNDIR="$(pwd)"

# --- Environment setup
RESUME=${RESUME:-yes}
SLEEP=${SLEEP:-1000}

echo "run-wsc.sh Environment"
echo "--------------------------"
echo "RUNDIR=$RUNDIR"
echo "UNSERVED_SRC=$UNSERVED_SRC"
echo "UNSERVED_JAR=$UNSERVED_JAR"
echo "RESUME=$RESUME"
echo "SLEEP=$SLEEP"
echo "--------------------------"

# Hardcoded
OUTDIR="$RUNDIR/run"
CLASSNAME=br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.WscRawExperimentRunner

# Validate some variables
if [ -z "$UNSERVED_SRC" ]; then echo "UNSERVED_SRC not set!"; exit 1; fi
if [ -z "$UNSERVED_JAR" ]; then echo "UNSERVED_JAR not set!"; exit 1; fi

if ! ( echo $RESUME | grep -E '(yes|no)' &>/dev/null ); then
  echo "RESUME must be either yes or no. \"$RESUME\" was given"; 
  exit 1
fi

# --- cleanup run dir if running from scratch
if [ "$RESUME" = "no" ]; then
  echo "Starting experiment from scratch"
  rm -fr "$OUTDIR"
  rm -f "results.csv"
fi
mkdir -p "$OUTDIR"

pushd "$UNSERVED_SRC"
git log -1 > "$OUTDIR/unserved-testbench.commit"
popd

java -cp "$UNSERVED_JAR" $CLASSNAME --design-csv "$RUNDIR/design.csv" \
    --results-csv "results.csv" \
    --result-objects-dir "$OUTDIR/results" --continue "$OUTDIR/state.json" \
    --sleep "$SLEEP" 2>&1 \
  | tee -a "$OUTDIR/log"
STATUS=$?
if [ "$STATUS" -ne 0 ]; then
  echo "unserved-testbench failed with code $STATUS"
fi
exit $STATUS


