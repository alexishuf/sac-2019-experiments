#!/bin/sh
set-cpufreq.sh
java -Xmx2g -Xms2g -jar ~/ufsc/configmate-wrapper/target/configmate-wrapper-1.0-SNAPSHOT.jar csv-runner --sleep 2000 --conf-dir ~/sources/configmate --results-csv ~/ufsc/configmate-wrapper-experiments/2018-05-16T2235/results.csv ~/ufsc/configmate-wrapper-experiments/2018-05-16T2235/design.csv
unset-cpufreq.sh
