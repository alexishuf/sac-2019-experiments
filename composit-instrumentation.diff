diff --git a/composit-cli/pom.xml b/composit-cli/pom.xml
index 67548a4..1aa0b0e 100644
--- a/composit-cli/pom.xml
+++ b/composit-cli/pom.xml
@@ -92,6 +92,7 @@
                     <descriptorRefs>
                         <descriptorRef>jar-with-dependencies</descriptorRef>
                     </descriptorRefs>
+                    <skipAssembly>false</skipAssembly>
                     <finalName>${project.artifactId}-${project.version}-full</finalName>
                     <appendAssemblyId>false</appendAssemblyId>
                 </configuration>
diff --git a/composit-cli/src/main/java/es/usc/citius/composit/cli/CompositCli.java b/composit-cli/src/main/java/es/usc/citius/composit/cli/CompositCli.java
index d1a9065..48b5307 100644
--- a/composit-cli/src/main/java/es/usc/citius/composit/cli/CompositCli.java
+++ b/composit-cli/src/main/java/es/usc/citius/composit/cli/CompositCli.java
@@ -89,6 +89,7 @@ public class CompositCli {
                 // Bind command impl
                 CliCommand instance = commandClass.newInstance();
                 bindings.put(instance.getCommandName(), instance);
+                cli.addCommand(instance.getCommandName(), instance);
                 log.debug("Command {} installed.", instance.getCommandName());
             } catch (InstantiationException e) {
                 log.warn("Exception during automatic command instantiation", e);
diff --git a/composit-cli/src/main/java/es/usc/citius/composit/cli/command/CompositionCommand.java b/composit-cli/src/main/java/es/usc/citius/composit/cli/command/CompositionCommand.java
index 2cc05de..dbdafe4 100644
--- a/composit-cli/src/main/java/es/usc/citius/composit/cli/command/CompositionCommand.java
+++ b/composit-cli/src/main/java/es/usc/citius/composit/cli/command/CompositionCommand.java
@@ -30,8 +30,12 @@ import es.usc.citius.composit.core.knowledge.Concept;
 import es.usc.citius.composit.core.matcher.logic.LogicMatchType;
 import es.usc.citius.composit.wsc08.data.WSCTest;
 
+import java.io.FileOutputStream;
+import java.io.PrintStream;
 import java.util.concurrent.TimeUnit;
 
+import static es.usc.citius.composit.core.composition.search.ComposIT.*;
+
 @Parameters(commandDescription = "Search for an optimal composition")
 public class CompositionCommand implements CliCommand {
 
@@ -62,22 +66,35 @@ public class CompositionCommand implements CliCommand {
         // Load dataset
         WSCTest.Dataset dataset = config.getTest().dataset();
 
-        ComposIT<Concept, Boolean> composit = new ComposIT<Concept, Boolean>(dataset.getDefaultCompositionProblem());
+        for (int i = 0; i < 3; i++) {
+            System.gc();
+            Thread.sleep(100);
 
-        // Configure search
-        if (config.isBackwardOptimization()){
-            composit.addOptimization(new BackwardMinimizationOptimizer<Concept, Boolean>());
-        }
-        if (config.isFunctionalDominance()){
-            composit.addOptimization(new FunctionalDominanceOptimizer<Concept, Boolean>());
-        }
+            ComposIT<Concept, Boolean> composit = new ComposIT<Concept, Boolean>(dataset.getDefaultCompositionProblem());
 
-        if (benchmarkCycles > 0){
-            benchmark(composit, dataset, benchmarkCycles);
-        } else {
-            composit.search(dataset.getRequest());
-        }
+            // Configure search
+            if (config.isBackwardOptimization()) {
+                composit.addOptimization(new BackwardMinimizationOptimizer<Concept, Boolean>());
+            }
+            if (config.isFunctionalDominance()) {
+                composit.addOptimization(new FunctionalDominanceOptimizer<Concept, Boolean>());
+            }
 
+            if (benchmarkCycles > 0) {
+                benchmark(composit, dataset, benchmarkCycles);
+            } else {
+                composit.search(dataset.getRequest());
+                if (i == 2) {
+                    String name = this.config.getTest().toString();
+                    int problem = Integer.parseInt(name.substring(name.length() - 2));
+                    PrintStream out = new PrintStream(new FileOutputStream("/tmp/composit-times"));
+                    out.printf("%d, %.3f, %.3f, %.3f, %d, %d, %d, %d\n", problem,
+                            graphConstruction, graphOptimization, composition,
+                            graphConstructionMem, graphOptimizationMem, compositionMem, maxMem);
+                    out.close();
+                }
+            }
+        }
     }
 
     private void benchmark(ComposIT<Concept, Boolean> composit, WSCTest.Dataset dataset, int cycles){
diff --git a/composit-core/src/main/java/es/usc/citius/composit/core/composition/search/ComposIT.java b/composit-core/src/main/java/es/usc/citius/composit/core/composition/search/ComposIT.java
index 35e3e80..362e87c 100644
--- a/composit-core/src/main/java/es/usc/citius/composit/core/composition/search/ComposIT.java
+++ b/composit-core/src/main/java/es/usc/citius/composit/core/composition/search/ComposIT.java
@@ -18,6 +18,7 @@
 package es.usc.citius.composit.core.composition.search;
 
 import com.google.common.base.Function;
+import com.google.common.base.Preconditions;
 import com.google.common.base.Stopwatch;
 import com.google.common.collect.Lists;
 import es.usc.citius.composit.core.composition.HashLeveledServices;
@@ -33,9 +34,12 @@ import es.usc.citius.lab.hipster.node.HeuristicNode;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 
-import java.util.LinkedList;
-import java.util.List;
-import java.util.Set;
+import java.util.*;
+import java.util.concurrent.TimeUnit;
+import java.util.concurrent.locks.Condition;
+import java.util.concurrent.locks.Lock;
+import java.util.concurrent.locks.ReentrantLock;
+import java.util.stream.Stream;
 
 /**
  * @author Pablo Rodríguez Mier <<a href="mailto:pablo.rodriguez.mier@usc.es">pablo.rodriguez.mier@usc.es</a>>
@@ -92,22 +96,34 @@ public final class ComposIT<E, T extends Comparable<T>> {
     }
 
     public Algorithms.Search<State<E>,HeuristicNode<State<E>,Double>>.Result search(Signature<E> request){
+        MemorySampler memorySampler = new MemorySampler();
+        memorySampler.start();
         // Create the 3-pass service match network.
         log.info("Initializing composition search problem...");
         // Composition starts with the request:
         Stopwatch compositionWatch = Stopwatch.createStarted();
         // Build the initial match graph network (first pass)
+        Stopwatch discoveryTimer = Stopwatch.createStarted();
         ServiceMatchNetwork<E, T> network = discoverer.search(request);
+        graphConstruction = discoveryTimer.elapsed(TimeUnit.MICROSECONDS)/1000.0;
+        graphConstructionMem = usedMemory();
         // Apply optimizations
         Stopwatch optWatch = Stopwatch.createStarted();
         for(NetworkOptimizer<E,T> opt : optimizations){
             network = opt.optimize(network);
         }
         optWatch.stop();
+        graphOptimization = optWatch.elapsed(TimeUnit.MICROSECONDS)/1000.0;
+        graphOptimizationMem = usedMemory();
         log.info("Graph optimizations done in {}", optWatch);
         log.info("Starting search over a network with {} levels and {} operations", network.numberOfLevels(), network.listOperations().size());
         // Run search over network
         Algorithms.Search<State<E>,HeuristicNode<State<E>,Double>>.Result searchResult = CompositSearch.create(network).search();
+        composition = searchResult.getStopwatch().elapsed(TimeUnit.MICROSECONDS)/1000.0;
+        compositionMem = usedMemory();
+        memorySampler.shutdown();
+        List<Long> mems = Arrays.asList(graphConstructionMem, graphOptimizationMem, compositionMem, maxMem);
+        maxMem = mems.get(mems.size()-1);
         log.info("Optimal composition search finished in {}", searchResult.getStopwatch().toString());
         log.debug("   Composition         : {}", searchResult.getOptimalPath());
         log.debug("   Total iterations    : {}", searchResult.getIterations());
@@ -124,4 +140,60 @@ public final class ComposIT<E, T extends Comparable<T>> {
         });
         return searchResult;
     }
+
+    public static long usedMemory() {
+        Runtime rt = Runtime.getRuntime();
+        return rt.totalMemory() - rt.freeMemory();
+    }
+
+    public class MemorySampler extends Thread {
+        private Lock lock = new ReentrantLock();
+        private boolean shutdownCalled = false;
+        private Condition stopping = lock.newCondition(), stopped = lock.newCondition();
+
+        @Override
+        public synchronized void start() {
+            Preconditions.checkState(!shutdownCalled);
+            shutdownCalled = false;
+            super.start();
+        }
+
+        @Override
+        public void run() {
+            lock.lock();
+            try {
+                maxMem = Math.max(maxMem, usedMemory());
+                while (!shutdownCalled && !stopping.await(10, TimeUnit.MILLISECONDS)) {
+                    maxMem = Math.max(maxMem, usedMemory());
+                }
+            } catch (InterruptedException ignored) {
+            } finally {
+                stopped.signal();
+                lock.unlock();
+            }
+        }
+
+        public void shutdown() {
+            lock.lock();
+            Preconditions.checkState(!shutdownCalled);
+            try {
+                Preconditions.checkState(isAlive() && !shutdownCalled);
+                shutdownCalled = true;
+                stopping.signalAll();
+                stopped.await();
+                maxMem = Math.max(maxMem, usedMemory());
+            } catch (InterruptedException ignored) {
+            } finally {
+                lock.unlock();
+            }
+        }
+    }
+
+    public static double graphConstruction = -1;
+    public static double graphOptimization = -1;
+    public static double composition = -1;
+    public static long graphConstructionMem = -1;
+    public static long graphOptimizationMem = -1;
+    public static long compositionMem = -1;
+    public static long maxMem = -1;
 }
diff --git a/pom.xml b/pom.xml
index 12f5b72..b038fa9 100644
--- a/pom.xml
+++ b/pom.xml
@@ -196,6 +196,11 @@
                     <groupId>org.apache.maven.plugins</groupId>
                     <artifactId>maven-assembly-plugin</artifactId>
                     <version>2.4</version>
+                    <configuration>
+                        <!-- Overridden on composit-cli.
+                             Fixes mvn package assembly:single on composit-parent -->
+                        <skipAssembly>true</skipAssembly>
+                    </configuration>
                 </plugin>
 
                 <plugin>
